import store from './store'



function byteToHex(byte) {
    const res = (byte >= 0 ? byte : (byte + 256)).toString(16).toUpperCase();
    return res.length < 2 ? '0'+res : res;
}

function byteArrayToHex(bytes) {
    let result = '';
    for(let i=0; i<bytes.length; i++) {
        result += byteToHex(bytes[i]);
    }
    return result;
}

function nfcTagWasRead(tag) {
    const code = byteArrayToHex(tag.id);
    store.dispatch('getCustomerInfo', {
        code,
        type: 'nfc'
    });
}

export function initNfcListener() {
    document.addEventListener('deviceready', function () {
        try {
            nfc.enabled(function() {

                nfc.addTagDiscoveredListener(
                    function(e ) {
                        nfcTagWasRead(e.tag);
                    },
                    function() {

                    },
                    function() {
                        alert("Ошибка регистрации обработчика событий NFC.");
                    }
                );

            }, function() {
                alert('NFC не доступен или отключен');
            });
        } catch(e) {
            alert(e);
        }
    }, false);

}