export function isActivated(state) {
    return state.accessToken !== null;
}

export function customerErrorDescription(state) {
    if(state.networkError && isActivated(state) && !state.customerInfo)
        return state.networkError;

    if(state.customerError === null)
        return null;
    switch(state.customerError) {
        case 'not_found':
            return 'Карта с таким кодом не найдена';
        case 'not_activated':
            return 'Карта не активирована';
        case 'no-transaction':
            return 'Транзакция не существует';
        default:
            return state.customerError;
    }
}

export function transactionErrorDescription(state) {
    if(state.networkError && state.customerInfo)
        return state.networkError;
    if(state.transactionError === null)
        return null;
    switch(state.transactionError) {
        case 'no-card':
            return 'Карта не существует или не активирована';
        case 'not-enough-bonuses':
            return 'Не достаточно баллов на карте';
        default:
            return state.transactionError;
    }
}

export function rollbackErrorDescription(state) {
    if(state.networkError)
        return state.networkError;

    if(state.rollbackError === null)
        return null;
    switch(state.rollbackError) {
        case 'no-transaction':
            return 'Не найдена покупака с таким кодом';
        case 'transaction-cancelled':
            return 'Покупка уже отменена';
        case 'transaction-applied':
            return 'Начисление баллов уже произведено, невозможно оформить возврат';
        default:
            return state.rollbackError;
    }
}