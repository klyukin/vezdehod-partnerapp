import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

import Bluebird from 'bluebird';
window.Promise = Bluebird;

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        activationPhoneVerified: false,
        accessToken: localStorage.getItem('access-token'),
        partnerName: localStorage.getItem('partner-name'),
        customerInfo: null,
        availablePromoevents: [],
        customerError: null,
        transactionId: null,
        transactionInfo: null,
        transactionError: null,
        transactionSuccess: false,

        rollbackError: null,

        dataLoading: false,
        networkError: null
    },
    getters,
    mutations,
    actions,
});