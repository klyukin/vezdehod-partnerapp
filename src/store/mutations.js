
export function activate(state, payload) {
    state.accessToken = payload.token;
    state.partnerName = payload.partnerName;
    localStorage.setItem('access-token', payload.token);
    localStorage.setItem('partner-name', payload.partnerName);
}

export function deactivate(state) {
    state.accessToken = null;
    localStorage.removeItem('access-token');
    cancelCustomer(state);
}

export function setCustomerInfo(state, info) {
    state.customerInfo = info;
    state.transactionId = null;
    state.transactionInfo = null;
    state.customerError = null;
}

export function customerError(state, errorCode) {
    state.customerError = errorCode;
}

export function cancelCustomer(state) {
    state.customerInfo = null;
    state.customerError = null;
    state.networkError = null;
    cancelTransaction(state);
}

export function cancelTransaction(state) {
    state.transactionId = null;
    state.transactionInfo = null;
    state.transactionError = null;
}

export function setTransactionInfo(state, info) {
    state.transactionId = info.id;
    state.transactionInfo = info.details;
    state.transactionError = null;
}

export function setTransactionError(state, errorCode) {
    state.transactionError = errorCode;
}

export function transactionCommitted(state) {
    state.transactionSuccess = true;
}

export function closeTransactionConfirmation(state) {
    state.transactionSuccess = false;
    cancelCustomer(state);
}

export function setRollbackSuccess(state) {
    state.transactionSuccess = true;
}

export function setRollbackError(state, errorCode) {
    state.rollbackError = errorCode;
}


export function startLoading(state) {
    state.dataLoading = true;
    state.networkError = null;
}
export function endLoading(state) {
    state.dataLoading = false;
    state.networkError = null;
}

export function loadingError(state, errorObj) {
    state.networkError = errorObj.errorMessage;
}


export function setAvailablePromoevents(state, promoevents) {
    state.availablePromoevents = promoevents;
}