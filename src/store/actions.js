import { request, HttpError } from '../helpers/request';
import moment from 'moment';

var lastPromoeventRequest = null;

async function makeRequest(commit, cb) {
    commit('startLoading');
    try {
        return await cb();
    } finally {
        commit('endLoading');
    }
}

// Transactions

export async function getCustomerInfo({commit, state}, model) {
    const url = `customer/by-code?code=${model.code}&type=${model.type}`;
    const resp = await makeRequest(commit, () => request(url, state.accessToken));
    if (resp instanceof HttpError) {
        if (resp.responseCode == 404) {
            commit('customerError', 'not_found');
            return;
        }
        if (resp.responseCode == 401 || resp.responseCode == 403) {
            alert('Необходима повторная привязка устройуства');
            commit('deactivate');
            return;
        }

        commit('loadingError', resp);
        return;
    }
    resp.code = model.code;
    resp.type = model.type;

    await loadPromoevents({commit, state}, resp.id);

    commit('setCustomerInfo', resp);
}

async function loadPromoevents({commit, state}, customerId) {
    const resp = await request('transactions/promoevents?customerId='+customerId, state.accessToken);
    if (resp instanceof HttpError) {
        return;
    }
    commit('setAvailablePromoevents', resp);
    lastPromoeventRequest = moment();
}

export async function createTransaction({commit, state}, model) {
    const url = 'transactions';
    const body = {
        card: state.customerInfo.code,
        type: state.customerInfo.type,
        amount: model.amount,
        paid_by_bonuses: model.paid_by_bonuses,
        promoevents: model.promoevents,
    };
    const resp = await makeRequest(commit, () => request(url, state.accessToken, {method: 'POST', body: body}));
    if (resp instanceof HttpError) {
        commit('loadingError', resp);
        return;
    }
    if(resp.success) {
        commit('setTransactionInfo', resp);
    } else {
        commit('setTransactionError', resp.error);
    }
}

export async function commitTransaction({commit, state}) {
    const url = 'transactions/commit';
    const body = {
        id: state.transactionId,
    };
    const resp = await makeRequest(commit, () => request(url, state.accessToken, {method: 'POST', body: body}));
    if (resp instanceof HttpError) {
        commit('loadingError', resp);
        return;
    }
    if(resp.success) {
        commit('transactionCommitted');
    } else {
        commit('setTransactionError', resp.error);
    }
}

export async function rollbackTransaction({commit, state}, model) {
    const url = 'transactions/rollback';
    const body = {
        code: model.code,
    };
    const resp = await makeRequest(commit, () => request(url, state.accessToken, {method: 'POST', body: body}));
    if (resp instanceof HttpError) {
        commit('loadingError', resp);
        return;
    }
    if(resp.success) {
        commit('setRollbackSuccess');
    } else {
        commit('setRollbackError', resp.error);
    }
}