const API_URL = 'http://api.karta-go.ru/api/';
//const API_URL = 'http://localhost:8080/api/';


export class HttpError {
    constructor(code) {
        this.responseCode = code;
    }

    get errorMessage() {
        if(this.responseCode < 0)
            return 'Ошибка получения данных от сервера';
        return `Код ошибки ${this.responseCode}`;
    }
}


export async function request(url, token, {method='GET', body}={}) {
    const options = {
        method: method,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };
    if(token) {
        options.headers.Authorization = 'Bearer ' + token;
    }
    if(body) {
        options.body = JSON.stringify(body);
    }
    try {
        const resp = await fetch(API_URL + url, options);
        if(resp.status != 200) {
            return new HttpError(resp.status);
        }
        return await resp.json();
    } catch(e) {
        console.log(e);
        //throw e;
        return new HttpError(-1);
    }
};

