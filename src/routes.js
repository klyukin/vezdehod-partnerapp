export default [
    {
        path: '/',
        component: require('./pages/transaction.vue')
    },
    {
        path: '/rollback/',
        component: require('./pages/rollback.vue')
    },
    {
        path: '/support/',
        component: require('./pages/support.vue')
    },
]